def expensive(arg1, arg2, _cache={}):
    if (arg1, arg2) in _cache:
        return _cache[(arg1, arg2)]

    # Расчёт значения
    result = arg1*arg2
    _cache[(arg1, arg2)] = result     # Кладём результат в кэш
    return result

for x in range(999999):
    expensive(1,2)
    expensive(3,5)
    expensive(1,2)